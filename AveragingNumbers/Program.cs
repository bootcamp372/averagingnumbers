﻿namespace AveragingNumbers
{
    internal class Program
    {
        static void Main(string[] args)
        {
            AvgNumbers();
        }


        static void AvgNumbers()
        {

            double number1, number2, number3, number4;

            Console.Write("Number 1: ");
            number1 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Number 2: ");
            number2 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Number 3: ");
            number3 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Number 4: ");
            number4 = Convert.ToDouble(Console.ReadLine());

            double avg = (number1 + number2 + number3 + number4) / 4;
            Console.WriteLine("The average of {0}, {1}, {2}, {3} is: {4} ",
         number1, number2, number3, number4, avg);
        }
    }


    
}