﻿namespace DistanceBetweenPoints
{
    internal class Program
    {
        static void Main(string[] args)
        {
            DistanceBetweenPoints();

        }

        static void DistanceBetweenPoints()
        {
            Int16 x1, x2, y1, y2;
            x1 = 2;
            x2 = 6;
            y1 = 3;
            y2 = 5;
            var distance = Math.Sqrt((Math.Pow(x1 - x2, 2) + Math.Pow(y1 - y2, 2)));
            Console.WriteLine("Distance is: " + distance);
        }


    }
}