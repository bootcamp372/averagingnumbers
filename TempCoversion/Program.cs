﻿namespace TempCoversion
{
    internal class Program
    {
        static void Main(string[] args)
        {
            TempConverter();

        }

        static void TempConverter()
        {
            double Fahrenheit;
            Console.Write("Enter a temperature in Fahrenheit: ");
            Fahrenheit = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Fahrenheit Temp:" + Fahrenheit);
            double Celsius = (Fahrenheit - 32) * 5 / 9;

            /*            double Celsius = (Fahrenheit - 32) * (.5556);*/
            Console.WriteLine("Celsius Temp = {0:0.0}", Celsius);

        }


    }
}