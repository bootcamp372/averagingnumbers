﻿using System.Xml.Linq;

namespace MortgageCalculator
{
    internal class Program
    {
        static void Main(string[] args)
        {
            GetAmount();
        }

        static void GetAmount()
        {
            double A = 0;
            Console.Write("How much are you borrowing?");
            try
            {
                A = Convert.ToDouble(Console.ReadLine());

                GetInterestRate(A);

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
            finally
            {
                Console.WriteLine("GetAmount function excecuted");
            }
        }

        public static void GetInterestRate(double A)
        {
            double MIR;

            try
            {
                Console.Write("What is your interest rate?");
                MIR = Convert.ToDouble(Console.ReadLine());
                GetLoanLength(MIR, A);

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
            finally
            {
                Console.WriteLine("GetInterestRate function executed");
            }
        }

        public static void GetLoanLength(double MIR, double A)
        {
            try
            {
                Console.Write("How long is your loan in years?");
                double loanLength = Convert.ToDouble(Console.ReadLine());

                CaculateMortgage(MIR, A, loanLength);

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
            finally
            {
                Console.WriteLine("GetLoanLength function executed");
            }
        }

        public static void CaculateMortgage(double MIR, double A, double loanLength)
        {
            MIR = MIR / 1200;
            double NP = loanLength * 12 * -1;

            double exp = 1 + MIR;
            double numerator = A * MIR;
            double denominator = 1 - (Math.Pow(1 + MIR, NP));
            double estimatedPayment = numerator / denominator;
            double totalPaid = estimatedPayment * loanLength * 12;
            double totalInterest = totalPaid - A;

            Console.WriteLine($"Your estimated payment is {estimatedPayment}!");
            Console.WriteLine($"Your paid {totalPaid} over the life of the loan.");
            Console.WriteLine($"Your total interest cost for the loan is {totalInterest}!");

            Console.Write("Do you want to get information on another loan? (Y/N)");
            string command = Console.ReadLine().ToUpper();

            while (command == "Y")
            {
                GetAmount();
                if (command == "N")
                {
                    break;
                }
                else
                {
                    Console.WriteLine(command);
                    Console.WriteLine("**Error: command must be Y or N");
                    Console.Write("Do you want to get information on another loan? (Y/N)");
                    command = Console.ReadLine().ToUpper();

                }
            }
        }
        }
}